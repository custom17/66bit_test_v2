import React, { createContext } from 'react'

const Context: React.Context<any> = createContext(null)

export default Context
