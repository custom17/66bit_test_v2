const convertTextToJSON = (text: string) => {
	let json: any[] = []

	let textArr = text.split('\n')
	textArr = textArr.filter((item: string) => !!item)

	textArr.forEach((string: string) => {
		const tmpSection = string.split('')
		const tmpKeyLine = string.split('=')
		if (tmpSection[0] === '[' && tmpSection[string.length - 1] === ']') {
			json.push({ name: string.replace(/^.|.$/g, ''), keys: [] })
		} else if (tmpKeyLine.length === 2 && json.length) {
			json[json.length - 1].keys.push({ name: tmpKeyLine[0].trim(), value: tmpKeyLine[1]?.trim() })
		}
	})

	return json
}

export default convertTextToJSON
