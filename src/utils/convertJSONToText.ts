const convertJSONToText = (json: any) => {
	let text = ''

	if (!json) return text

	json.forEach((section: any, sectionIndex: number) => {
		text += `[${section.name}]`
		text += `\n`
		section.keys.forEach((key: any, keyIndex: number) => {
			if (key.value) {
				text += `${key.name} = ${key.value || ''}`
				text += '\n'
			} else {
				text += `${key.name}`
			}
		})
		if (sectionIndex < json.length - 1) text += '\n'
	})

	return text
}

export default convertJSONToText
