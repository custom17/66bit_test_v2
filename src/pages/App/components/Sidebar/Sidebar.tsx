import React, { useContext } from 'react'
import classNames from 'classnames'

import Context from '@context/Context'

import styles from './Sidebar.module.scss'

const Sidebar = () => {
	const { list, selectFile, visibleMenu, setVisibleMenu } = useContext(Context)

	return (
		<div className={classNames(styles.root, { [styles.visible]: visibleMenu })}>
			{list.map((file: any) => (
				<div
					key={file.id}
					className={styles.file}
					onClick={() => {
						selectFile(file)
						setVisibleMenu(false)
					}}
				>
					<span>{file.name}</span>
				</div>
			))}
		</div>
	)
}

export default Sidebar
