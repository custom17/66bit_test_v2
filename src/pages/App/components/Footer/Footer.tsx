import React, { ChangeEvent, FC, useContext } from 'react'

import Context from '@context/Context'

import styles from './Footer.module.scss'

interface FooterProps {}

const Footer: FC<FooterProps> = () => {
	const { file, saveFile, clearFile, chaneFileName } = useContext(Context)

	if (!file) return <div className={styles.root} />

	return (
		<div className={styles.root}>
			<label>
				Название файла{' '}
				<input
					value={file.name}
					onChange={(value: ChangeEvent<HTMLInputElement>) => {
						chaneFileName(value.target.value)
					}}
				/>
			</label>
			<button disabled={!file.name} onClick={saveFile}>
				Сохранить файл
			</button>
			<button onClick={clearFile}>Отменить</button>
		</div>
	)
}

export default Footer
