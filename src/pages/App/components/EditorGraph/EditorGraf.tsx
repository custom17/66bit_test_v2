import React, { ChangeEvent, FC, useContext } from 'react'
import classNames from 'classnames'

import Context from '@context/Context'

import styles from './EditorGraph.module.scss'

interface EditorGraphProps {}

const EditorGraph: FC<EditorGraphProps> = () => {
	const {
		editorHeight,
		file,
		sections,
		createNewSection,
		changeSectionName,
		createNewKey,
		changeKey,
		editVersion,
	} = useContext(Context)

	if (!file) return null

	return (
		<div
			className={classNames(styles.root, { [styles.visible]: editVersion })}
			style={{ height: editorHeight - 24 }}
		>
			<div className={styles.content}>
				{sections.map((section: any, sectionIndex: number) => {
					return (
						<div key={sectionIndex} className={styles.section}>
							<div className={styles.keys}>
								<div className={styles.sectionName}>
									<label>
										Название секции{' '}
										<input
											type={'text'}
											placeholder={'Название секции'}
											value={section.name}
											onChange={(value: ChangeEvent<HTMLInputElement>) => {
												changeSectionName(sectionIndex, value.target.value)
											}}
										/>
									</label>
								</div>
								{section.keys.map((key: any, keyIndex: number) => {
									return (
										<div key={keyIndex} className={styles.key}>
											<div>
												<input
													type={'text'}
													placeholder={'Ключ'}
													value={key.name}
													onChange={(value: ChangeEvent<HTMLInputElement>) => {
														changeKey(sectionIndex, keyIndex, 'name', value.target.value)
													}}
												/>
											</div>
											=
											<div>
												<input
													placeholder={'Значение'}
													type={Number(key.value) ? 'number' : 'text'}
													value={key.value}
													onChange={(value: ChangeEvent<HTMLInputElement>) => {
														changeKey(sectionIndex, keyIndex, 'value', value.target.value)
													}}
												/>
											</div>
										</div>
									)
								})}
							</div>
							<button onClick={() => createNewKey(sectionIndex)}>Добавить ключ</button>
						</div>
					)
				})}
			</div>
			<button onClick={createNewSection}>Добавить секцию</button>
		</div>
	)
}

export default EditorGraph
