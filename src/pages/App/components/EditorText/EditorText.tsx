import React, { FC, useContext } from 'react'
import classNames from 'classnames'

import convertJSONToText from '@utils/convertJSONToText'

import Context from '@context/Context'

import styles from './EditorText.module.scss'

interface EditorTextProps {}

const EditorText: FC<EditorTextProps> = () => {
	const { editorHeight, file, sections, changeFileText, editVersion } = useContext(Context)

	if (!file) return null

	return (
		<div
			className={classNames(styles.root, { [styles.visible]: !editVersion })}
			style={{ height: editorHeight - 24 }}
		>
			<textarea
				className={styles.textarea}
				value={convertJSONToText(sections)}
				onChange={(value: any) => {
					changeFileText(value.target.value)
				}}
			/>
		</div>
	)
}

export default EditorText
