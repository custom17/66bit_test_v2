import React, { FC, useContext, useEffect, useRef } from 'react'

import Context from '@context/Context'

import EditorText from '@pages/App/components/EditorText'
import EditorGraph from '@pages/App/components/EditorGraph'

import styles from './Editor.module.scss'

interface EditorProps {}

const Editor: FC<EditorProps> = () => {
	const contentRef = useRef<null | HTMLDivElement>(null)
	const { setEditorHeight } = useContext(Context)

	useEffect(() => {
		setEditorHeight(JSON.stringify(contentRef.current?.clientHeight))
	}, [])

	return (
		<div className={styles.root} ref={contentRef}>
			<EditorText />
			<EditorGraph />
		</div>
	)
}

export default Editor
