import React, { FC } from 'react'

import Header from '@pages/App/components/Header'
import Editor from '@pages/App/components/Editor'
import Footer from '@pages/App/components/Footer'

import styles from './Content.module.scss'

interface ContentProps {}

const Content: FC<ContentProps> = () => {
	return (
		<div className={styles.root}>
			<Header />
			<Editor />
			<Footer />
		</div>
	)
}

export default Content
