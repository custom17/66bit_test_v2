import React, { FC, useCallback, useContext } from 'react'

import Context from '@context/Context'

import styles from './Header.module.scss'

interface HeaderProps {}

const Header: FC<HeaderProps> = () => {
	const { file, createNewFile, setVisibleMenu, editVersion, setEditVersion } = useContext(Context)

	return (
		<div className={styles.root}>
			<div className={styles.menu}>
				<button onClick={() => setVisibleMenu(true)}>Меню</button>
			</div>
			<div className={styles.menu}>
				<button onClick={() => setEditVersion(!editVersion)}>
					На {editVersion ? 'текстовый' : 'графический'}
				</button>
			</div>
			<div className={styles.new}>
				<button disabled={file} onClick={createNewFile}>
					Создать файл
				</button>
			</div>
		</div>
	)
}

export default Header
