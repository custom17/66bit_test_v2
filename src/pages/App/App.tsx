import React, { useCallback, useEffect, useState } from 'react'

import generateId from '@utils/generateId'

import Context from '@context/Context'
import Sidebar from '@pages/App/components/Sidebar'
import Content from '@pages/App/components/Content'

import styles from './App.module.scss'
import convertTextToJSON from '@utils/convertTextToJSON'

const App = () => {
	const [editorHeight, setEditorHeight] = useState<number>(0)
	const [visibleMenu, setVisibleMenu] = useState<boolean>(false)
	const [editVersion, setEditVersion] = useState<boolean>(false)
	const [list, setList] = useState<any>([])
	const [file, setFile] = useState<any>(null)
	const [sections, setSections] = useState<any>([])

	useEffect(() => {
		const filesList = localStorage.getItem('@FilesList')

		if (filesList) {
			setList(JSON.parse(filesList))
		} else {
			localStorage.setItem('@FilesList', '[]')
			setList([])
		}
	}, [])

	useEffect(() => {
		localStorage.setItem('@FilesList', JSON.stringify(list))
	}, [list])

	//File - start
	const createNewFile = useCallback(() => {
		const id = generateId()
		setFile({
			id,
			name: `file_${id}.ini`,
		})
	}, [])

	const clearFile = useCallback(() => {
		setFile(null)
	}, [])

	const saveFile = useCallback(() => {
		const index = list.findIndex((item: any) => item.id === file.id)

		if (index === -1) {
			setList((prev: any) => [...prev, { ...file, sections }])
		} else {
			list.splice(index, 1, { ...file, sections })
			setList([...list])
		}
		setFile(null)
		setSections([])
	}, [file, sections])

	const chaneFileName = useCallback((value: string) => {
		setFile((prev: any) => {
			return { ...prev, name: value }
		})
	}, [])

	const selectFile = useCallback((file: any) => {
		setFile(file)
		setSections(file.sections)
	}, [])
	//File - end

	//Section - start
	const createNewSection = useCallback(() => {
		setSections((prev: any) => [...prev, { name: 'Section', keys: [] }])
	}, [])

	const changeSectionName = useCallback(
		(index: number, value: string) => {
			sections.splice(index, 1, {
				...sections[index],
				name: value,
			})

			setSections([...sections])
		},
		[sections]
	)
	//Section - end

	//Key - start
	const createNewKey = useCallback(
		(sectionIndex: number) => {
			sections.splice(sectionIndex, 1, {
				...sections[sectionIndex],
				keys: [...sections[sectionIndex].keys, { name: `key_${generateId()}`, value: null }],
			})

			setSections([...sections])
		},
		[sections]
	)

	const changeKey = useCallback(
		(sectionIndex: number, keyIndex: number, field: string, value: string) => {
			const keys = sections[sectionIndex].keys

			keys.splice(keyIndex, 1, { ...keys[keyIndex], [field]: value })

			sections.splice(sectionIndex, 1, {
				...sections[sectionIndex],
				keys,
			})

			setSections([...sections])
		},
		[sections]
	)
	//Key - end

	const changeFileText = useCallback((text: string) => {
		setSections(convertTextToJSON(text))
	}, [])

	const context = {
		editorHeight,
		list,
		file,
		sections,
		setEditorHeight,
		createNewFile,
		clearFile,
		saveFile,
		chaneFileName,
		selectFile,
		createNewSection,
		changeSectionName,
		createNewKey,
		changeKey,
		changeFileText,
		visibleMenu,
		setVisibleMenu,
		editVersion,
		setEditVersion,
	}

	return (
		<Context.Provider value={context}>
			<div className={styles.root}>
				<div className={styles.content}>
					<div className={styles.container}>
						<Sidebar />
						<Content />
					</div>
				</div>
			</div>
		</Context.Provider>
	)
}

export default App
